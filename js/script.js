$(function () {
  $("#menu_btn").on("click", function () {
    if ($("body").hasClass("open")) {
      $("body").removeClass("open");
      $(this).children("img").attr('src', './img/menu_btn.svg');
    } else {
      $("body").addClass("open");
      $(this).children("img").attr('src', './img/menu_close_btn.svg');
    };
  });
});

$('.slick01').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: true,
  infinite: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
      }
    }
  ]
})